package Utulisateur;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import La_cuillereENTITY.Type;
import La_cuillereENTITY.Utilisateur;
import La_cuillereSESSION.TypeFacadeLocal;
import La_cuillereSESSION.UtilisateurFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
public class autentification extends HttpServlet {

    
    @EJB 
            UtilisateurFacadeLocal usbr;
    @EJB 
        
            TypeFacadeLocal tsbr;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Autentification</title>");            
            out.println("</head>");
            out.println("<body>");
            String nom = request.getParameter("form-first-name");
            String prenom = request.getParameter("form-last-name");
            String telephone = request.getParameter("form-telephone");
            String adresse = request.getParameter("form-adresse");
            String mail = request.getParameter("form-email");
            String mdp = request.getParameter("form-password");        
            int id_type  = Integer.parseInt(request.getParameter("optradio")) ;
            
           // Type type = tsbr.find(id_type);
            Type type = new Type (1,"Utilisateur");
            int i = usbr.count()+1;
            Utilisateur a = new Utilisateur(i,nom, prenom, telephone, adresse, mail, mdp, type);
            usbr.create(a);
            response.sendRedirect("boot.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
