/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utulisateur;

import La_cuillereENTITY.Restaurant;
import La_cuillereENTITY.Type;
import La_cuillereENTITY.Utilisateur;
import La_cuillereSESSION.RestaurantFacadeLocal;
import La_cuillereSESSION.UtilisateurFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author dell
 */
public class login extends HttpServlet {

    
    @EJB 
            UtilisateurFacadeLocal usbr;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>  Hello </title>");   
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println();
            Utilisateur a = usbr.findUtilisateur(request.getParameter("form-username"));
            if (a == null)
            {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Erreur</title>");            
            out.println("</head>");
            }else
            {
                if(a.getMotDePasse().equals(request.getParameter("form-password")))
                {
                    
                     HttpSession session = request.getSession(true);
                     session.setAttribute("Nom", a.getNom());
                     session.setAttribute("prenom", a.getPrenom());
                     session.setAttribute("mail", a.getMail());
                     session.setAttribute("adresse", a.getAdresse());
                     session.setMaxInactiveInterval(300);
                     request.getRequestDispatcher("boot.jsp").forward(request, response);
                     response.sendRedirect("boot.html");
                   
                }else out.println("<title>erreur 2</title>"); 
                
            }
            
            out.println("<body>");
            out.println("<h1>Servlet login at LA SORTIE " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
