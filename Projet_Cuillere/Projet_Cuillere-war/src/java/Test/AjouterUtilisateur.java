/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import La_cuillereENTITY.Restaurant;
import La_cuillereENTITY.Type;
import La_cuillereENTITY.Utilisateur;
import La_cuillereSESSION.RestaurantFacadeLocal;
import La_cuillereSESSION.TypeFacadeLocal;
import La_cuillereSESSION.UtilisateurFacadeLocal;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dell
 */
public class AjouterUtilisateur extends HttpServlet {
    

    
    @EJB
       UtilisateurFacadeLocal usbr;
    @EJB
        RestaurantFacadeLocal  rsbr;
    @EJB
        TypeFacadeLocal  tsbr;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AjouterLivre</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("Test /n /n");
            
           
            Type type = new Type (1,"Client");
            Utilisateur ahlem= new Utilisateur(1,"Ahlem","Nom","0202020202","adresses","mail@esi.dz","MDP",type);
            Utilisateur nazim= new Utilisateur(2,"Nazim","Kechida","0202020202","adresses","mail","MDP",type);
            Utilisateur yassmine= new Utilisateur(3,"Yasmine","K","0202020202","adresses","mail","MDP",type);
            usbr.create(nazim);
            usbr.create(ahlem);
            usbr.create(yassmine);
            yassmine= usbr.findUtilisateur("mail@esi.dz");
            out.println(yassmine.getMail());
            /*
            out.println("Ahlem : " + ahlem.getNom());
            out.println("Le prénon du 2 :" + usbr.findUtilisateur(2).getPrenom());
            out.println("Nom Avant : " + yassmine.getPrenom());
            usbr.editUtilisateur(3, "nom","KAOUA");
            out.println("Nom aprés : " + yassmine.getPrenom());
            */
           // Restaurant rest1 =new Restaurant(10, "Crepe", "41 rue guersant", "01201201", "Helo@esi.dz", "Salade", "Salade",10);
            
            usbr.editUtilisateur(ahlem,"LOL");
            
           // out.println(rest1.getNom());
            
           // rsbr.createRestaurant(rest1);
          // rest1= rsbr.findNameRestaurant("Salade");
           // out.println("   ALORS : " + rest1.getNom());
            
            
            out.println("<h1>Servlet AjouterLivre at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
            
        }
    }
    
}
