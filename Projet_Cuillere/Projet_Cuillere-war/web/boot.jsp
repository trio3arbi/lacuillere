<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>




<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">La cuillère</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="#">Acceuil</a></li>
      <li><a href="#">Faire une reservation</a></li>
      <li><a href="#">Consulter mon panier</a></li>
    </ul>

<% 
session = request.getSession(false);
if(session!=null)
{ %>

 <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> Se connecter</a></li>
      <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Créer un compte</a></li>
</ul>

<% } %>

<% if(session==null) { %>

 <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Se deconnecter</a></li>
</ul>

<% } %>

   
  </div>
</nav>



<div class="container">
<div class="jumbotron text-center">
  <h1>La cuillère</h1>
  <p>Où désirez vous manger aujourd'hui?</p> 
   <form class="navbar-form navbar-center">
  <div class="form-group">
        <input type="text" class="form-control input-lg" placeholder="...">
      </div>
     
      <button type="submit" class="btn btn-info">Rechercher</button>
  </form>
</div>
  <div class="row">
    <div class="col-sm-4">
   
      <h3>Top des restaurants</h3>
      <p>The .img-responsive class makes the image scale nicely to the parent element (resize the browser window to see the effect):</p>
      <div class="container"><img class="img-responsive" src="assets\img\image1.jpg" alt="Chania" width="350" height="200"> </div>
    </div>
    <div class="col-sm-4">
      <h3>Nouveauté</h3>
      <p>The .img-responsive class makes the image scale nicely to the parent element (resize the browser window to see the effect):</p>
      <div class="container"><img class="img-responsive" src="assets\img\image2.jpg" alt="Chania" width="350" height="200"> </div>
    </div>
    <div class="col-sm-4">
      <h3>Restaurant à priximité</h3>        
      <p>The .img-responsive class makes the image scale nicely to the parent element (resize the browser window to see the effect):</p>
      <div class="container"><img class="img-responsive" src="assets\img\image3.jpg" alt="Chania" width="350" height="200"> </div> 
    </div>
  </div>
</div>

</body>
</html>
