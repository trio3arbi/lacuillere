/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Utilisateur;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


/**
 *
 * @author dell
 */
@Stateless
public class UtilisateurFacade extends AbstractFacade<Utilisateur> implements UtilisateurFacadeLocal {

    @PersistenceContext(unitName = "Projet_Cuillere-ejbPU")
    private EntityManager em;

    
    public UtilisateurFacade() {
        super(Utilisateur.class);
    }
    

    public UtilisateurFacade(Class<Utilisateur> entityClass) {
        super(entityClass);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void create(Utilisateur utilisateur)
    {
       em.persist(utilisateur);
       
    }
/*
    @Override
    public void editUtilisateur(int id, String attr, String val) {
       //Utilisateur utilisateur = findUtilisateur(id);
       Query query = em.createQuery("UPDATE public.utilisateur u SET u."+attr+"='"+val+"' WHERE u.id='"+id+"'");
       query.executeUpdate();
       
    }
*/
     @Override
    public void editUtilisateur(Utilisateur u,String t) {
       
        u.setNom(t);
        em.merge(u);
       
    }

    @Override
    public Utilisateur findUtilisateur(int id) {
       
    return em.find(Utilisateur.class, id);
 
    }

    @Override
    public Utilisateur findUtilisateur(String mail) {
        Query q = em.createNamedQuery("Utilisateur.findByMail");
        q.setParameter("mail", mail);
        return (Utilisateur) q.getSingleResult();
    }

   public int count(){
   
      List<Utilisateur> a = findAll();
      return a.size();
   }
    
}
