/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Annonce;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author dell
 */
@Stateless
public class AnnonceFacade extends AbstractFacade<Annonce> implements AnnonceFacadeLocal {

    @PersistenceContext(unitName = "Projet_Cuillere-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AnnonceFacade() {
        super(Annonce.class);
    }
    
    public void createAnnonce (Annonce annonce){
    
        em.persist(annonce);
    }
    
    public List<Annonce> findAll(){
    
        Query query = em.createNamedQuery("Annonce.findAll");
        return query.getResultList();
    }
    
    public Annonce find(int id){
        return em.find(Annonce.class, id);
    }
    
      @Override
    public void editMenuAnnonce(int id, int menu) {
        
        Annonce a = em.find(Annonce.class, id);
        a.setFkMenuId(menu);
        em.merge(a);
       
    }
}
