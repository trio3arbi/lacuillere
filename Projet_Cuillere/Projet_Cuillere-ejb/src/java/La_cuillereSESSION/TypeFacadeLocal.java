/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Type;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface TypeFacadeLocal {

    void createType(Type type);

    void edit(Type type);

    void remove(Type type);

    Type find(int id);

    List<Type> findAll();

    List<Type> findRange(int[] range);

    int count();

 
    
}
