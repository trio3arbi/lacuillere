/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package La_cuillereSESSION;

import La_cuillereENTITY.Plage;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dell
 */
@Stateless
public class PlageFacade extends AbstractFacade<Plage> implements PlageFacadeLocal {

    @PersistenceContext(unitName = "Projet_Cuillere-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PlageFacade() {
        super(Plage.class);
    }

    @Override
    public void createPlage(Plage plage) {
        em.persist(plage);
    }
    
}
