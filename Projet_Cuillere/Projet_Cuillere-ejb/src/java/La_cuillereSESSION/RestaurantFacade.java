/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Restaurant;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author dell
 */
@Stateless
public class RestaurantFacade extends AbstractFacade<Restaurant> implements RestaurantFacadeLocal {

    @PersistenceContext(unitName = "Projet_Cuillere-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RestaurantFacade() {
        super(Restaurant.class);
    }
    
    public void createRestaurant (Restaurant restaurant){
    
        em.persist(restaurant);
    }
    
    public Restaurant find(int id){
    
        return em.find(Restaurant.class, id);
    }
    /*
    public Restaurant findName(String nom){
    
        Query query;
        query = em.createQuery("SELECT r FROM Restaurant r WHERE r.categorie = :nom");
      //  return (Restaurant) em.createNamedQuery("Restaurant.findByNom").getParameterValue(nom);
      query.getSingleResult();
      return (Restaurant) query.getSingleResult();  
      Restaurant.findByNom
    }
    */
     public Restaurant findNameRestaurant(String nom){
    
       Query query = em.createNamedQuery("Restaurant.findByNom");
       query.setParameter("nom",nom);
      return (Restaurant) query.getSingleResult();  
      
    }

    @Override
    public void editPlace(int restaurant, int i) {
        Restaurant r = em.find(Restaurant.class, restaurant);
        r.setplace(r.getplace()-i);
        em.merge(r);
    }
    
}
