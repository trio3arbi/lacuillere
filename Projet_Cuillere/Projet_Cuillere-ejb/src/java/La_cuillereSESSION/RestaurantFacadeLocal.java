/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Restaurant;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface RestaurantFacadeLocal {

    void createRestaurant(Restaurant restaurant);

    void edit(Restaurant restaurant);
    
    void editPlace(int restaurant,int i);

    void remove(Restaurant restaurant);

    Restaurant find(Object id);
    
    Restaurant findNameRestaurant(String nom);

    List<Restaurant> findAll();

    List<Restaurant> findRange(int[] range);

    int count();
    
}
