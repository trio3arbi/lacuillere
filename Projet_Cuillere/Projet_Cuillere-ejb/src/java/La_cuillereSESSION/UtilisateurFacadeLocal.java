/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Utilisateur;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface UtilisateurFacadeLocal {

    void create(Utilisateur utilisateur);

//    void editUtilisateur(int id,String attr, String val);

    void editUtilisateur(Utilisateur u,String t);
    
    void remove(Utilisateur utilisateur);

    Utilisateur findUtilisateur(int id);
    
    Utilisateur findUtilisateur(String mail);

    List<Utilisateur> findAll();

    List<Utilisateur> findRange(int[] range);

    int count();
    
}
