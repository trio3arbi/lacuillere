/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Reservation;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author dell
 */
@Stateless
public class ReservationFacade extends AbstractFacade<Reservation> implements ReservationFacadeLocal {

    @PersistenceContext(unitName = "Projet_Cuillere-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ReservationFacade() {
        super(Reservation.class);
    }

    @Override
    public void createReservation(Reservation reservation) {
        em.persist(reservation);
    }
    
    public Reservation find(int id) {
       
    return em.find(Reservation.class, id);
 
    }

    @Override
    public Reservation findbyRestaurant(int id_restaurant) {

        Query query = em.createNamedQuery("Reservation.findByFkRestaurantId");
        query.setParameter("FkRestaurantId",id_restaurant);
        return (Reservation) query.getSingleResult();  
   
    }
    
    public void remove(int id){
    
        Reservation reservation = find(id);
        em.remove(reservation);
    }
    
    
    
}
