/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Plage;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface PlageFacadeLocal {

    void createPlage(Plage plage);

    void edit(Plage plage);

    void remove(Plage plage);

    Plage find(Object id);

    List<Plage> findAll();

    List<Plage> findRange(int[] range);

    int count();
    
}
