/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereSESSION;

import La_cuillereENTITY.Annonce;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface AnnonceFacadeLocal {

    void createAnnonce (Annonce annonce);

    void editMenuAnnonce(int id, int menu);

    void remove(Annonce annonce);

    Annonce find(int id);

    List<Annonce> findAll();

    List<Annonce> findRange(int[] range);

    int count();
    
}
