/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereENTITY;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dell
 */
@Entity
@Table(name = "annonce")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Annonce.findAll", query = "SELECT a FROM Annonce a")
    , @NamedQuery(name = "Annonce.findById", query = "SELECT a FROM Annonce a WHERE a.id = :id")
    , @NamedQuery(name = "Annonce.findByFkRestaurateurId", query = "SELECT a FROM Annonce a WHERE a.fkRestaurateurId = :fkRestaurateurId")
    , @NamedQuery(name = "Annonce.findByFkRestaurantId", query = "SELECT a FROM Annonce a WHERE a.fkRestaurantId = :fkRestaurantId")
    , @NamedQuery(name = "Annonce.findByFkMenuId", query = "SELECT a FROM Annonce a WHERE a.fkMenuId = :fkMenuId")
    , @NamedQuery(name = "Annonce.findByDateAnnonce", query = "SELECT a FROM Annonce a WHERE a.dateAnnonce = :dateAnnonce")
    , @NamedQuery(name = "Annonce.findByReduction", query = "SELECT a FROM Annonce a WHERE a.reduction = :reduction")})
public class Annonce implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_restaurateur_id")
    private int fkRestaurateurId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_restaurant_id")
    private int fkRestaurantId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_menu_id")
    private int fkMenuId;
    @Column(name = "date_annonce")
    @Temporal(TemporalType.DATE)
    private Date dateAnnonce;
    @Column(name = "reduction")
    private Integer reduction;
    @JoinColumn(name = "fk_plage_id", referencedColumnName = "id")
    @ManyToOne
    private Plage fkPlageId;

    public Annonce() {
    }

    public Annonce(int id,int id_restaurateur, int id_restaurant, int id_menu, Date date, int reduction,Plage plage) {
        
        this.id=id;
        this.fkRestaurateurId=id_restaurateur;
        this.fkRestaurantId=id_restaurant;
        this.dateAnnonce=date;
        this.reduction=reduction;
        this.fkMenuId=id_menu;
        this.fkPlageId=plage; 
    }
    
    public Annonce(Integer id) {
        this.id = id;
    }

    public Annonce(Integer id, int fkRestaurateurId, int fkRestaurantId, int fkMenuId) {
        this.id = id;
        this.fkRestaurateurId = fkRestaurateurId;
        this.fkRestaurantId = fkRestaurantId;
        this.fkMenuId = fkMenuId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFkRestaurateurId() {
        return fkRestaurateurId;
    }

    public void setFkRestaurateurId(int fkRestaurateurId) {
        this.fkRestaurateurId = fkRestaurateurId;
    }

    public int getFkRestaurantId() {
        return fkRestaurantId;
    }

    public void setFkRestaurantId(int fkRestaurantId) {
        this.fkRestaurantId = fkRestaurantId;
    }

    public int getFkMenuId() {
        return fkMenuId;
    }

    public void setFkMenuId(int fkMenuId) {
        this.fkMenuId = fkMenuId;
    }

    public Date getDateAnnonce() {
        return dateAnnonce;
    }

    public void setDateAnnonce(Date dateAnnonce) {
        this.dateAnnonce = dateAnnonce;
    }

    public Integer getReduction() {
        return reduction;
    }

    public void setReduction(Integer reduction) {
        this.reduction = reduction;
    }

    public Plage getFkPlageId() {
        return fkPlageId;
    }

    public void setFkPlageId(Plage fkPlageId) {
        this.fkPlageId = fkPlageId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Annonce)) {
            return false;
        }
        Annonce other = (Annonce) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "La_cuillereENTITY.Annonce[ id=" + id + " ]";
    }
    
}
