/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereENTITY;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dell
 */
@Entity
@Table(name = "reservation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reservation.findAll", query = "SELECT r FROM Reservation r")
    , @NamedQuery(name = "Reservation.findById", query = "SELECT r FROM Reservation r WHERE r.id = :id")
    , @NamedQuery(name = "Reservation.findByFkPlageId", query = "SELECT r FROM Reservation r WHERE r.fkPlageId = :fkPlageId")
    , @NamedQuery(name = "Reservation.findByNbrePlace", query = "SELECT r FROM Reservation r WHERE r.nbrePlace = :nbrePlace")
    , @NamedQuery(name = "Reservation.findByFkMenuId", query = "SELECT r FROM Reservation r WHERE r.fkMenuId = :fkMenuId")
    , @NamedQuery(name = "Reservation.findByFkRestaurantId", query = "SELECT r FROM Reservation r WHERE r.fkRestaurantId = :fkRestaurantId")})
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Column(name = "fk_plage_id")
    private Integer fkPlageId;
    @Column(name = "nbre_place")
    private Integer nbrePlace;
    @Column(name = "fk_menu_id")
    private Integer fkMenuId;
    @Column(name = "fk_restaurant_id")
    private Integer fkRestaurantId;

    public Reservation() {
    }
    
    public Reservation(int id, int plage, int nbr_place, int id_menu, int id_restaurant) {
        
        this.id=id;
        this.fkPlageId=plage;
        this.nbrePlace=nbr_place;
        this.fkMenuId=id_menu;
        this.fkRestaurantId=id_restaurant;
    }
    

    public Reservation(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFkPlageId() {
        return fkPlageId;
    }

    public void setFkPlageId(Integer fkPlageId) {
        this.fkPlageId = fkPlageId;
    }

    public Integer getNbrePlace() {
        return nbrePlace;
    }

    public void setNbrePlace(Integer nbrePlace) {
        this.nbrePlace = nbrePlace;
    }

    public Integer getFkMenuId() {
        return fkMenuId;
    }

    public void setFkMenuId(Integer fkMenuId) {
        this.fkMenuId = fkMenuId;
    }

    public Integer getFkRestaurantId() {
        return fkRestaurantId;
    }

    public void setFkRestaurantId(Integer fkRestaurantId) {
        this.fkRestaurantId = fkRestaurantId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reservation)) {
            return false;
        }
        Reservation other = (Reservation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "La_cuillereENTITY.Reservation[ id=" + id + " ]";
    }
    
}
