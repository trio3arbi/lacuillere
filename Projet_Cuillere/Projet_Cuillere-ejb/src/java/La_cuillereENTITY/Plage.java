/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package La_cuillereENTITY;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dell
 */
@Entity
@Table(name = "plage")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Plage.findAll", query = "SELECT p FROM Plage p")
    , @NamedQuery(name = "Plage.findById", query = "SELECT p FROM Plage p WHERE p.id = :id")
    , @NamedQuery(name = "Plage.findByHeureDebut", query = "SELECT p FROM Plage p WHERE p.heureDebut = :heureDebut")
    , @NamedQuery(name = "Plage.findByHeureFin", query = "SELECT p FROM Plage p WHERE p.heureFin = :heureFin")
    , @NamedQuery(name = "Plage.findByNbreDisponible", query = "SELECT p FROM Plage p WHERE p.nbreDisponible = :nbreDisponible")})
public class Plage implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 30)
    @Column(name = "heure_debut")
    private String heureDebut;
    @Size(max = 30)
    @Column(name = "heure_fin")
    private String heureFin;
    @Column(name = "nbre_disponible")
    private Integer nbreDisponible;
    @OneToMany(mappedBy = "fkPlageId")
    private Collection<Annonce> annonceCollection;

    public Plage() {
    }

    public Plage(int id, String heure_debut, String heure_fin, int nbre_dispo,Collection<Annonce> annonces) {
        this.id=id;
        this.heureDebut=heure_debut;
        this.heureFin=heure_fin;
        this.nbreDisponible=nbre_dispo;
        this.annonceCollection=annonces;
        
    }
    public Plage(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
    }

    public String getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(String heureFin) {
        this.heureFin = heureFin;
    }

    public Integer getNbreDisponible() {
        return nbreDisponible;
    }

    public void setNbreDisponible(Integer nbreDisponible) {
        this.nbreDisponible = nbreDisponible;
    }

    @XmlTransient
    public Collection<Annonce> getAnnonceCollection() {
        return annonceCollection;
    }

    public void setAnnonceCollection(Collection<Annonce> annonceCollection) {
        this.annonceCollection = annonceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Plage)) {
            return false;
        }
        Plage other = (Plage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "La_cuillereENTITY.Plage[ id=" + id + " ]";
    }
    
}
